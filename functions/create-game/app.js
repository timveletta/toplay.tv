// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
const AWS = require("aws-sdk");
const uuidv4 = require("uuid/v4");

let config = {
  region: "ap-southeast-2"
};

if (process.env.AWS_SAM_LOCAL) {
  config.endpoint = "http://docker.for.mac.localhost:8000";
}

const DocumentClient = new AWS.DynamoDB.DocumentClient(config);

const DB_TABLE = "toplay-game";
/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */
exports.lambdaHandler = async (event, context) => {
  const generateId = () => {
    return uuidv4().slice(0, 4);
  };
  const isUniqueId = async id => {
    let params = {
      TableName: DB_TABLE,
      KeyConditionExpression: "ID = :id",
      ExpressionAttributeValues: {
        ":id": id
      }
    };
    // if this fails, I want it to bubble up
    let result = await DocumentClient.query(params).promise();
    return result.Count == 0;
  };

  let id;
  do {
    id = generateId();
  } while (!(await isUniqueId(id)));

  let result;
  try {
    let params = {
      TableName: DB_TABLE,
      Item: {
        ID: id,
        Players: []
      },
      Expected: {
        ID: {
          Exists: false
        }
      }
    };
    result = await DocumentClient.put(params).promise();
  } catch (err) {
    return { statusCode: 400, body: JSON.stringify(err) };
  }
  return {
    statusCode: 200,
    body: JSON.stringify({ id })
  };
};
