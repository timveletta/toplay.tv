#!/bin/bash

docker network create lambda-local

docker run -d -v "$PWD":/dynamodb_local_db -p 8000:8000 --network lambda-local amazon/dynamodb-local
sam local start-api --docker-network lambda-local --port 4000
