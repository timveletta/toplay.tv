const AWS = require("aws-sdk");
const uuidv4 = require("uuid/v4");

let config = {
  region: "ap-southeast-2"
};

if (process.env.AWS_SAM_LOCAL) {
  config.endpoint = "http://docker.for.mac.localhost:8000";
}

const DocumentClient = new AWS.DynamoDB.DocumentClient(config);

const DB_TABLE = "toplay-game";
/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */
exports.lambdaHandler = async (event, context) => {
  // assert incoming parameters
  const body = event.body && JSON.parse(event.body);
  const player = {
    id: uuidv4(),
    name: body.name
  };

  let params = {
    TableName: DB_TABLE,
    Key: {
      ID: body.gameId
    },
    UpdateExpression:
      "SET #players = list_append(if_not_exists(#players, :empty_list), :newPlayer)",
    ExpressionAttributeNames: {
      "#players": "Players"
    },
    ExpressionAttributeValues: {
      ":empty_list": [],
      ":newPlayer": [player]
    },
    ReturnValues: "ALL_NEW"
  };

  let result;
  try {
    result = await DocumentClient.update(params).promise();
  } catch (err) {
    console.log(err);
    return err;
  }
  return { statusCode: 200, body: JSON.stringify(player) };
};
