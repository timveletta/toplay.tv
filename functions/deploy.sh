sam package --template-file template.yaml --s3-bucket to-play.tv --output-template-file packaged.yaml

sam deploy --template-file ./packaged.yaml --stack-name toPlayTV --capabilities CAPABILITY_IAM