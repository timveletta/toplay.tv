Feature: Create Game

    In order to play the game
    As a host
    I want to be able to create a new game

    Scenario: Successfully create a new game
        Given I am a host
        When I create a new game
        Then a new game should be created