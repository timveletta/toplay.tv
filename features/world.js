const { setWorldConstructor, setDefaultTimeout } = require("cucumber");

setDefaultTimeout(60 * 1000);

class World {
  constructor() {
    this.user = "";
    this.gameId = null;
    this.url = process.env.PUBLIC_URL;
    this.playerData = {};
  }

  setHost() {
    this.user = "host";
  }

  setPlayer() {
    this.user = "player";
  }
}

setWorldConstructor(World);
setDefaultTimeout(10 * 1000);
