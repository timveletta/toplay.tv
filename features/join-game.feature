Feature: Join Game

    In order to play the game
    As a player
    I want to be able to join a game

    Scenario: Successfully join a game
        Given I am a player
        When a host creates a game
        And the game code is shown
        When I enter the room code
        And My name is "Tim"
        And I join the game
        Then I should join the game
        And My name should be shown

    Scenario: Attempt to join a game that doesn't exist
        Given I am a player
        When I enter the room code "1234"
        And My name is "Tim"
        And I join the game
        Then I should not join the game