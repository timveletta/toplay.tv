const { Given, When, Then } = require("cucumber");
const got = require("got");
const expect = require("chai").expect;

Given("I am a host", function() {
  this.setHost();
});

Given("I am a player", function() {
  this.setPlayer();
});

When("I create a new game", async function() {
  const result = await got.post(`${this.url}/${this.user}/game`);
  expect(result.statusCode).to.equal(200);
  const data = JSON.parse(result.body);
  expect(data.id).to.exist;
  this.gameId = data.id;
});

When("a host creates a game", async function() {
  const result = await got.post(`${this.url}/host/game`);
  expect(result.statusCode).to.equal(200);
  const data = JSON.parse(result.body);
  expect(data.id).to.exist;
  this.gameId = data.id;
});

When("the game code is shown", function() {
  // TODO more of a UI test
  expect(this.gameId).to.exist;
});

When("the game has ended", function() {
  // Write code here that turns the phrase above into concrete actions
  return "pending";
});

When("I enter the room code", function() {
  this.playerData.roomId = this.gameId;
});

When("My name is {string}", function(name) {
  this.playerData.name = name;
});

When("I join the game", async function() {
  const result = await got.post(`${this.url}/${this.user}/game/join`, {
    body: JSON.stringify(this.playerData)
  });
  expect(result.statusCode).to.equal(200);
  const data = JSON.parse(result.body);
  expect(data.id).to.exist;
  this.playerData.id = data.id;
});

When("I enter the room code {string}", function(code) {
  this.playerData.roomId = code;
});

Then("a new game should be created", async function() {
  const result = await got.get(`${this.url}/${this.user}/game/${this.gameId}`);
  expect(result.statusCode).to.equal(200);
  const data = JSON.parse(result.body);
  expect(data.id).to.equal(this.gameId);
});

Then("I should join the game", async function() {
  const result = await got.get(`${this.url}/${this.user}/game/${this.gameId}`);
  expect(result.statusCode).to.equal(200);
  const data = JSON.parse(result.body);
  expect(data.players)
    .to.be.an("array")
    .that.deep.includes(this.playerData);
});

Then("I should not join the game", async function() {
  const result = await got.get(`${this.url}/${this.user}/game/${this.gameId}`);
  expect(result.statusCode).to.equal(200);
  const data = JSON.parse(result.body);
  expect(data.players)
    .to.be.an("array")
    .that.does.not.include(this.playerData);
});

Then("My name should be shown", function() {
  // TODO more of a UI test
});
